function getEntryById(id, cb) {
	var byId = new XMLHttpRequest();
	byId.open('GET', 'http://localhost:3333/entry/' + id, true);
	byId.send();
	byId.onreadystatechange = function() {
		if (byId.readyState === 4 && byId.status == 200) {
			cb(JSON.parse(byId.responseText));
		}
	}
};
function getAllEntries(cb) {
	var all = new XMLHttpRequest();
	all.open('GET', 'http://localhost:3333/entry', true);
	all.send();
	all.onreadystatechange = function() {
		if (all.readyState === 4 && all.status == 200) {
			cb(JSON.parse(all.responseText));
		}
	}
};
function deleteEntry(id, trigger) {
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', 'http://localhost:3333/entry', true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(id);
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == 200) {
			trigger();
		}
	}
};
function addEntry(data, trigger) {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'http://localhost:3333/entry', true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(data);
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == 200) {
			trigger();
		}
	}
};
function updateEntry(update, trigger) {
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', 'http://localhost:3333/entry', true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(update);
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == 200) {
			trigger();
		}
	}
};
function getTpl(file, cb){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'http://127.0.0.1:8080/data/' + file, true);
	xhr.send();
	var arr = [];
	xhr.onreadystatechange = function() {
		if (xhr.readyState === 4 && xhr.status == 200) {
			cb(xhr.responseText);
		}
	}
	window.arrr = arr;
};