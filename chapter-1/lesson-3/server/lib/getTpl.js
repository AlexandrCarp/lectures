(function(){
		window.Class = function Class(tpl, collection) {
		this.tpl = tpl;
		this.collection = collection;
		this.output = ''
		this.parse = function(tpl, object) {
			var repl = [];
			var keyTmp = Object.keys(object);
			var res = tpl;
			for (var key in object) {
				repl.push('{{' + [key] + '}}');
			};
			for (var i = 0; i < repl.length; i++) {
				res = res.replace(repl[i], object[keyTmp[i]]); 
			}
			return res;
		};
		this.start = function() {
			if (collection.length === undefined) {
				this.output = this.parse(this.tpl, this.collection);
			} else {
				for (var i = 0; i < this.collection.length; i++) {
					this.output += this.parse(this.tpl, this.collection[i]);
				}
			}
		};
		this.render = function() {
			this.start(tpl, collection);
		};
		this.getTpl = function() {
			return this.output;
		}
		};
}());