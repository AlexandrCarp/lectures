(function(){
	function Class() {
		this.arr = [];
		this.reqEv = function(data, fun){
			var obj = {
				data: arguments[0],
				fun: arguments[1]
			};
			this.arr.push(obj);
		};
		this.trigger = function(data){
			var obj1 = {data: arguments[0]};
			var res = _.where(this.arr, obj1);
			res[0].fun();
		};
		this.del = function(data) {
			var obj1 = {data: arguments[0]};
			for (var i = 0; i < this.arr.length; i++) {
				if (obj1.data === this.arr[i].data) {
					this.arr.splice(i, 1);
				}
			}
		}
	}
	window.Req = Class;
})();