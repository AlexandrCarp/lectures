
(function() {
	var a = {
		each: function each (list, fun) {

			var arr = Object.keys(list);
				for (var i = 0; i < arr.length; i++) {
					fun(list[arr[i]], arr[i]);
				}

			},
		map: function map (list, fun) {
			var newArr = [];

			for (var i = 0; i < list.length; i++) {
				newArr.push(fun(list[i]));
			}

			return newArr;
		},
		find: function find (list, iterator) {

			for (var i = 0; i < list.length; i++) {
				if (iterator(list[i])) {
					return list[i];
				}
			}

		},
		filter: function filter (list, condition) {
			var res = [];

			for (var i = 0; i < list.length; i++) {
				if (condition(list[i])) {
					res.push(list[i]);
				}
			}

			return res;
		},
		contains: function contains (list, value) {

			for (var i = 0; i < list.length; i++) {
				if (list[i] === value) {
					return true;
				}
			}

			return false;
		},
		pluck: function pluck (list, propertyName) {
			var newArr = [];

			for (var i = 0; i < list.length; i++) {
				if (list[i][propertyName]) {
					newArr.push(list[i][propertyName]);
				}
			}

			return newArr;
		},
		values: function values (obj) {
			var newArr = [];
			var arr = Object.keys(obj);

			for (var i = 0; i < arr.length; i ++) {
				newArr.push(obj[arr[i]]);
			}

			return newArr;
		},
		where: function where (arr, properties) {
			var newArr = [];
			var count = 0;
			var length = 0;
			var arrObj = Object.keys(properties);

			for (var i = 0; i < arr.length; i++) {
				for (var j = 0; j < arrObj.length; j++) {
					if (arr[i][arrObj[j]] === properties[arrObj[j]]) {
						count++;
						length++;
					} else {
						length++;
					}
				}
				if (count === length) {
					newArr.push(arr[i]);
				}
				count = 0;
				length = 0;
			}

			return newArr;
		},
		findWhere: function findWhere (arr, properties) {
			var newArr = [];
			var count = 0;
			var length = 0;
			var arrObj = Object.keys(properties);

			for (var i = 0; i < arr.length; i++) {
				for (var j = 0; j < arrObj.length; j++) {
					if (arr[i][arrObj[j]] === properties[arrObj[j]]) {
						count++;
						length++;
					} else {
						length++;
					}
				}
				if (count === length) {
					newArr.push(arr[i]);
					return newArr;
				}
			}
		}
	};
	window._ = a;
})();
